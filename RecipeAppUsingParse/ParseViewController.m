//
//  ParseViewController.m
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ParseViewController.h"
#import "Recipe.h"

@interface ParseViewController ()

@end

@implementation ParseViewController

- (id) initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self){
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = NO;        
    }
    return self;
}

- (PFQuery *)queryForTable{
    PFQuery *query = [PFQuery queryWithClassName: @"Recipe"];
    if ([self.objects count] == 0){
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"createdAt"];
    return query;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject*)object{

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    //configure the cell to show the name of the recipe
    cell.textLabel.text = [object objectForKey:@"name"];  //this works
    return cell;
}

// method for tableView didSelectRowAtIndexPath
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    RecipeDetailViewController *rdvc=[[RecipeDetailViewController alloc]init];
    rdvc.theRecipeObject = [self.objects objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:rdvc animated:YES];
    //NSLog(@"%@", rdvc.theRecipeObject);  //this works
}



- (void)viewDidLoad {
    [super viewDidLoad];
    //create an add button at the nav bar; (for adding a new recipe)
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewRecipe:)];
    self.navigationItem.rightBarButtonItem = addButton;
}

// to push to new recipe view controller
- (void)insertNewRecipe:(id)sender{
    addNewRecipeViewController *add=[[addNewRecipeViewController alloc]init];
    [self.navigationController pushViewController:add animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadObjects];
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
