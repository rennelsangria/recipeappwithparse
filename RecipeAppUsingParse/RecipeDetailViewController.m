//
//  RecipeDetailViewController.m
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//  this page shows the details of the recipe

#import "RecipeDetailViewController.h"

@interface RecipeDetailViewController ()
//- (void)configureView;
@end

@implementation RecipeDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    //create the label to display the name of the recipe
    UILabel* nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 30)];
    nameLbl.text = [self.theRecipeObject objectForKey:@"name"];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];

// attempt to put image
//    PFImageView* iv = [[PFImageView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, 300)];
//    iv= [UIImage imageNamed:@"..."];
//    iv.file = [theRecipeObject objectForKey:@"imageFile"];
//    [iv loadInBackground];
    
    //create the textview to display the ingredients and instructions for the recipe
    UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height)];
    txtView.textColor = [UIColor brownColor];
    txtView.text = [self.theRecipeObject objectForKey:@"description"];
    [self.view addSubview:txtView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
