//
//  addNewRecipeViewController.m
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "addNewRecipeViewController.h"
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>

@interface addNewRecipeViewController ()


@end

@implementation addNewRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
//create label for the recipe name
    UILabel* nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, self.view.frame.size.width, 30)];
    nameLbl.text = @"Recipe Name:";
    nameLbl.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:nameLbl];

//create textfield for the recipe name
    recipeName = [[UITextField alloc] initWithFrame:CGRectMake(10, 120, 300, 40)];
    recipeName.borderStyle = UITextBorderStyleRoundedRect;
    recipeName.font = [UIFont systemFontOfSize:15];
    recipeName.placeholder = @"enter recipe name";
    recipeName.autocorrectionType = UITextAutocorrectionTypeNo;
    recipeName.keyboardType = UIKeyboardTypeDefault;
    recipeName.returnKeyType = UIReturnKeyDefault;
    recipeName.clearButtonMode = UITextFieldViewModeWhileEditing;
    recipeName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:recipeName];
    
 //create the label for the recipe description
    UILabel* descLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 200, self.view.frame.size.width, 30)];
    descLbl.text = @"Description:";
    descLbl.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:descLbl];
 
 //create the textview of the recipe description
    CGRect textViewFrame = CGRectMake(10, 250, self.view.frame.size.width - 20, 300);
    recipeDescription = [[UITextView alloc] initWithFrame:textViewFrame];
    recipeDescription.backgroundColor = [UIColor yellowColor];
    recipeDescription.returnKeyType = UIReturnKeyDone;
    recipeDescription.layer.borderWidth = 2.3;
    recipeDescription.layer.cornerRadius = 5;
    [self.view addSubview:recipeDescription];
    
    
 //create a save button at the nav bar
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithTitle:@"Save"
                                                                  style:UIBarButtonItemStyleDone
                                                                 target:self
                                                                 action:@selector(saveClicked:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    
}

//save method that uses REST POST call
- (void)saveClicked:(id)sender{
    //REST HTTP POST REQUEST
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://parse.com/1/classes/Recipe"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"Di0xVursvey53iIB5paRR3vJO8whMhaeWtiOo8Nm" forHTTPHeaderField:@"X-Parse-Application-Id"];
        [request setValue:@"O7PWRpk1dcjJh4htljK7vyvyg4up6qW3iRiyfqhR" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    
        NSDictionary* diction = @{@"name":recipeName.text, @"description":recipeDescription.text};
        
        NSError* error;
        NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:diction
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
        [request setHTTPBody:dataFromDict];
        
        NSURLResponse *requestResponse;
        NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
        
        NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
        NSLog(@"requestReply: %@", requestReply);
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
