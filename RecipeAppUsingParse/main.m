//
//  main.m
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/20/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
