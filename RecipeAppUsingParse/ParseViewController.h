//
//  ParseViewController.h
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import "RecipeDetailViewController.h"
#import "addNewRecipeViewController.h"


@interface ParseViewController : PFQueryTableViewController

@end
