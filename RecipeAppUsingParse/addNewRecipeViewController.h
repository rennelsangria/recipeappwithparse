//
//  addNewRecipeViewController.h
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ParseViewController.h"

@interface addNewRecipeViewController : UIViewController{
    
        UITextField* recipeName;
        UITextView* recipeDescription;
    
}
-(IBAction)saveClicked:(id)sender;

@end
