//
//  AppDelegate.h
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/20/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

