//
//  RecipeDetailViewController.h
//  RecipeAppUsingParse
//
//  Created by Rennel Sangria on 6/22/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface RecipeDetailViewController : UIViewController
@property (strong,nonatomic) PFObject *theRecipeObject;


@end
